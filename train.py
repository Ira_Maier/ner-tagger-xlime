#!/usr/bin/python3
# -*- coding: utf-8 -*-
import importlib, re, statistics
from torch.utils.data import DataLoader, Subset, ConcatDataset
from seqeval.metrics import precision_score, recall_score, accuracy_score, f1_score, classification_report

from helpers import *
from helpers_2 import *
from main_model import *
from corpora import *
from test_model import *
from create_files import *

'''
'''

log = open(train_file, "w+", encoding="utf-8")

f1_scores = [0.0]

def save_checkpoint(val_f1, model):
    '''
    saves model when f1 has been improved
    :param val_f1:
    :param model:
    :return:
    '''
    print('saved model with', val_f1, ' at epoch', epoch)
    log.write('{} {} {} {} {}'.format('saved model with', val_f1, ' at epoch', epoch, '\n'))
    torch.save(model.state_dict(), results_dir+'/checkpoint_'+str(epoch)+'_'+str(val_f1)+'.pt')
    #torch.save(optimizer.state_dict(), results_dir + '/optimizer_' + str(epoch) + '_' + str(val_f1) + '.pt')

def get_checkpoint():
    checkpoints = [f for f in os.listdir(results_dir) if 'checkpoint' in f]
    max_results = [s.replace('.pt', '').rsplit('_', 1)[1] for s in checkpoints]
    checkpoint = None
    for c in checkpoints:
        if str(max(max_results)) in c:
            checkpoint = c
    return checkpoint

class EarlyStopping():
    def __init__(self, patience):
        self.last_saved_epoch = 0
        self.patience = patience

def train():
    for x, y in train_list:
        model.zero_grad()
        loss = model.neg_log_likelihood(x, y)
        loss.backward()
        optimizer.step()

def dev():
    f1, gold_labels, all_preds_string, low_prob, avg_prob, max_prob = calc_f1(dev_list, model)
    # print('gold labels', gold_labels)
    # print('pred labels', all_preds_string)

    f1 = round((f1*100), 2) #convert to % like float
    print('F1', f1)
    log.write('F1 '+str(f1)+'\n')
    #save best model + early stopping
    if f1_scores: #and last_saved_epoch
        if f1 > max(f1_scores):
            #print('saving with:', f1, 'and current epoch: ',epoch)
            save_checkpoint(f1, model)
            early_bird.last_saved_epoch = epoch
    f1_scores.append(f1)

    print('lowest, avg, max viterbi probabilities:',low_prob, avg_prob, max_prob)
    log.write('probs: '+str(low_prob)+' '+str(avg_prob)+' '+str(max_prob)+'\n')

if __name__ == '__main__':
    #print(results_dir) # initialize results_dir
    importlib.invalidate_caches()

    before = time()

    #retrain from checkpoint
    retrain = False

    early_bird = EarlyStopping(10)

    model = crf(idx2word = idx2word, idx2char = idx2char, tag2idx=ner2idx, word_embedding=word_embedding)  # vocab_size 12923 todo

    if retrain == True:
        #checkpoint = torch.load('results/30.06_04:39/checkpoint_35_61.67.pt')
        checkpoint = torch.load('results/20.07_23:35/checkpoint_3_61.8.pt')
        model.load_state_dict(checkpoint)
        log.write('retraining from model')
    model = model.to(device) # define model device before optimizer
    #print(model) #for layers
    if next(model.parameters()).is_cuda == False: print('not running on GPU')

    optimizer = optim.SGD(model.parameters(), lr=lr, weight_decay=weight_decay)

    # save model parameters to file
    if train_loader.dataset == xlime_train:
        log.write('xlime  \n')  # + twitter api corpus
    else:
        log.write('twitter api corpus, with size '+str(len(train_loader))+'\n')
    log.write('trainable embedding? ' + str(model.word_embedding.weight.requires_grad) + '\n')

    #print(len(train_list), len(dev_list), len(test_list))

    for epoch in range(50):
        print('epoch number', epoch)
        log.write('{} {} {}'.format('epoch number', epoch, '\n'))
        train()
        log.write('validation: \n')
        dev()
        print('\n')
        log.write('\n')
        # early stopping:
        if epoch - early_bird.last_saved_epoch > early_bird.patience:
            log.write('{} {} {} {} {} {}'.format('\n early stopping exit with F1: ', f1_scores[-1], 'and current epoch: ', epoch,
                                                 'last saved epoch', early_bird.last_saved_epoch))
            print('early stopped with:', f1_scores[-1])
            # f1 = test(results_dir, get_checkpoint(), test_list)
            # print('test F1: ', f1)
            # log.write('\n test F1: '+str(f1))
            after = time()
            print('time elapsed:', str(round(((after - before) / 60)/60)) + ' h')
            log.write('\n time elapsed: ' + str(round(((after - before) / 60)/60)) + ' h')
            log.close()
            sys.exit()


