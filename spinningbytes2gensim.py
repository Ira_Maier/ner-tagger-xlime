import gensim

word_vectors_s = gensim.models.KeyedVectors.load_word2vec_format('embeddings/embed_tweets_de_300D_fasttext', binary=False)
word_vectors_s.save('embeddings/spinningbytes_gensim.gensim')
#
# word_vectors_f = gensim.models.KeyedVectors.load_word2vec_format('embeddings/cc.de.300.bin', binary=True)
# word_vectors_f.save('embeddings/fasttext_gensim.gensim')