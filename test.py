import unittest

from helpers import *
from helpers_2 import *
from corpora import *
# from train import *
from flair_model import *

class TestService(unittest.TestCase):
    def test_seed(self):
        '''
        test, if same numbers are produced
        :return:
        '''
        rnd = random.random()
        self.assertEqual(rnd, rnd)

    def test_data(self):
        '''
        :return:
        '''
        # save tweets in dictionaries:
        train_dict = {i: e[0].tolist() for i, e in enumerate(train_list)}
        dev_dict = {i: e[0].tolist() for i, e in enumerate(dev_list)}
        test_dict = {i: e[0].tolist() for i, e in enumerate(test_list)}

        #train_dict_api =  {i: e[0].tolist() for i, e in enumerate(train_list_api)}

        flair_train_dict = {i: x for i, x in enumerate(xlime_train_flair)}
        flair_dev_dict = {i: x for i, x in enumerate(xlime_dev_flair)}
        flair_test_dict = {i: x for i, x in enumerate(xlime_test_flair)}

        # test, if train, dev & test sets are unique
        self.assertFalse(train_dict == dev_dict)
        self.assertFalse(train_dict == test_dict)
        self.assertFalse(test_dict == dev_dict)

        # self.assertFalse(train_dict_api == dev_dict)
        # self.assertFalse(train_dict_api == test_dict)

        self.assertFalse(flair_dev_dict == flair_train_dict)
        self.assertFalse(flair_test_dict == flair_dev_dict)
        self.assertFalse(flair_train_dict == flair_test_dict)


    def test_embedding(self):
        '''
        test, if embedding size = vocabulary size
        :return:
        '''
        self.assertEqual(word_embedding.num_embeddings, len(word2idx))

    def test_vocab(self):
        '''
        test, if vocabulary for tweets from api contains all words from xlime corpus
        :return:
        '''
        xlime_last_word = idx2word_xlime[12677]
        api_word = idx2word_api[12677]
        size_xlime_x = [len(x) for x in xlime.x]
        size_xlime_y = [len(y) for y in xlime.y]
        size_api_x = [len(x) for x in api.x]
        size_api_y = [len(y) for y in api.y]
        self.assertEqual(xlime_last_word, api_word)
        self.assertEqual(len(idx2word),84206)
        self.assertEqual(len(idx2char), 912)
        self.assertEqual(len(api.x), 11026)
        self.assertEqual(size_xlime_x, size_xlime_y)
        self.assertEqual(size_api_x, size_api_y)

if __name__ == '__main__':
    unittest.main(verbosity=2)