#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
from datetime import datetime

now = datetime.now()
datum = now.strftime("%d.%m_%H:%M")
datum_short = now.strftime("%d:%m")

# create folder for saving checkpoints, results, predictions
results_dir = "results/"+now.strftime("%d.%m_%H:%M")
if os.path.exists(results_dir) == False:
    os.makedirs(results_dir)
train_file = results_dir+"/train_" + datum + ".txt"
