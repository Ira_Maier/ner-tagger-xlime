import pickle

import main_model

#write predicted NER tags to .txt file for evaluating with Conll03 script
#python3 conlleval_perl.py < conll_test_gold.txt

def make_conll_file(gold_token, gold_ner, pred,  output_name):
    '''

    :param pred: nested list of sentences with predicted tags
    :param test: nested list of sentences with gold tags
    :return:
    '''
    output = open(output_name,"w", encoding="utf-8")
    for s in list(zip(gold_token, gold_ner, pred)):
        for t in list(zip(s[0],s[1], s[2])):
            tok = params['idx2word'][t[0].item()]
            pos = 'POS-fake'
            gold = t[1]
            pred = t[2] #iob
            output.write(tok+' '+pos+' '+gold+' '+pred+'\n')
        output.write('\n')

if __name__ == '__main__':
    with open("results/predictions_28:05:10:32.p", "rb") as pred_file, open("data.p", "rb") as gold_file:
        preds = pickle.load(pred_file)
        params = pickle.load(gold_file)
        make_conll_file(params['test sentences'],  params['test ner'], preds, 'conll_test_gold.txt')