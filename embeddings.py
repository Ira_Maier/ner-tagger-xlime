#!/usr/bin/python3
# -*- coding: utf-8 -*-
import importlib, gc
from sys import getsizeof

#from elmoformanylangs import Embedder
from fastText import load_model

from helpers import *

importlib.invalidate_caches()

'''
split files for saving memory:
split -d -l 100000 spinningbytes_embed_tweets_de_300D_fasttext spinningbytes/
'''
#get pretrained word embeddings
sp_weight_matrix = torch.tensor(np.zeros((len(word2idx), 300)), dtype=torch.float) # shape [vocab size, 300]

## get spinningbytes vectors
for f in os.listdir('embeddings/spinningbytes'):
    with open('embeddings/spinningbytes/'+f) as file:
        readlines = [r.split() for r in file.readlines()[1:]]
        for r in readlines:
            if r[0] in word2idx:
                vectors = [float(x) for x in r[1:]]
                sp_weight_matrix[word2idx[r[0]]] = torch.tensor(vectors, dtype=torch.float)

            if r[0].capitalize() in word2idx:
                vectors = [float(x) for x in r[1:]]
                sp_weight_matrix[word2idx[r[0].capitalize()]] = torch.tensor(vectors, dtype=torch.float)

fasttext_model = load_model('embeddings/cc.de.300.bin')

fasttext_weight_matrix = torch.tensor(np.zeros((len(word2idx), 300)), dtype=torch.float)

for k, v in word2idx.items():
    fasttext_weight_matrix[v] = torch.tensor(fasttext_model.get_word_vector(k), dtype=torch.float)

# concat word vectors
all_weights = torch.cat((sp_weight_matrix, fasttext_weight_matrix), dim=1)

word_embedding = nn.Embedding(len(word2idx), all_weights.shape[1])
word_embedding.load_state_dict({'weight': all_weights})

with open('embeddings/word embedding api.pt', 'wb') as f:
      torch.save(word_embedding, f)