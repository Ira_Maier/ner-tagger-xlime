import torch

'''
save a subset of tweets
'''

with open('data/tweets_de.txt', 'r') as reader:
    lines = reader.readlines()  # 87170
    lines = set(lines)  # remove duplicates --> advertisement
    lines = list(lines)  # 51053
    lines = lines[:11029]  # 4x xlime size

torch.save(lines, 'data/tweets_corpus.pt')