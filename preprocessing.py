#!/usr/bin/python3
# -*- coding: utf-8 -*-
import torch

from helpers_0 import *

'''
1. split sentences into nested lists of sentences and tags
2. remove #
2. convert strings to idx
3. convert every sentence into a tensor
'''

def convert_data(filename):
    '''

    :param filename: file with original corpus
    :return: nested lists with sentences and pos, ner, sentiment labels; both as String and integer representation
    '''
    with open(filename, encoding='utf-8') as file:

        word2idx, ner2idx, char2idx, pos2idx, sentiment2idx = {}, {}, {}, {}, {}
        readlines = [r.split() for r in file.readlines()[1:]]
        current_doc_id = readlines[1][2]
        # for all sentences
        sentences, sentences_idx, ners, ners_idx, poses, poses_idx, sentiments, sentiments_idx = [], [], [], [], [], [], [], []
        # for a single sentence
        sent_words, sent_idx, sent_ners, ner_idx, sent_poses, pos_idx, sent_sentiments, sentiment_idx = [], [], [], [], [], [], [], []
        for r in readlines:
            word = r[0]
            word = word.replace('#', '').strip()
            if word:
                sentiment = r[3]
                pos = r[4]
                ner = r[5]
                if ner == 'B-LOCATION': ner = 'B-LOC'
                if ner == 'I-LOCATION': ner = 'I-LOC'
                if ner == 'B-PERSON': ner = 'B-PER'
                if ner == 'I-PERSON': ner = 'I-PER'
                # check if new tweet begins
                if r[2] == current_doc_id:
                    sent_words.append(word)
                    sent_ners.append(ner)
                    sent_poses.append(pos)
                    sent_sentiments.append(sentiment)

                    if word not in word2idx:
                        word2idx[word] = len(word2idx)
                    sent_idx.append(word2idx[word])
                    if ner not in ner2idx: ner2idx[ner] = len(ner2idx)
                    ner_idx.append(ner2idx[ner])
                    if pos not in pos2idx:
                        pos2idx[pos] = len(pos2idx)
                    pos_idx.append(pos2idx[pos])
                    if sentiment not in sentiment2idx:
                        sentiment2idx[sentiment] = len(sentiment2idx)
                    sentiment_idx.append(sentiment2idx[sentiment])

                else:
                    current_doc_id = r[2]
                    sentences.append(sent_words)
                    ners.append(sent_ners)
                    poses.append(sent_poses)
                    sentiments.append(sent_sentiments)
                    sentences_idx.append(sent_idx)
                    ners_idx.append(ner_idx)
                    poses_idx.append(pos_idx)
                    sentiments_idx.append(sentiment_idx)
                    sent_words, sent_idx, sent_ners, ner_idx, sent_poses, pos_idx, sent_sentiments, sentiment_idx = [], [], [], [], [], [], [], []
                for c in word:
                    if c not in char2idx:
                        char2idx[c] = len(char2idx)
        sentences.append(sent_words)
        ners.append(sent_ners)
        poses.append(sent_poses)
        sentiments.append(sent_sentiments)
        sentences_idx.append(sent_idx)
        ners_idx.append(ner_idx)
        poses_idx.append(pos_idx)
        sentiments_idx.append(sentiment_idx)

        idx2word = {idx: w for w, idx in word2idx.items()}
        idx2ner = {idx: w for w, idx in ner2idx.items()}
        idx2char = {idx: w for w, idx in char2idx.items()}
        return sentences, ners, sentences_idx, ners_idx, word2idx, idx2word, ner2idx, idx2ner, char2idx, idx2char

if __name__ == '__main__':
    sentences, ners, sentences_idx, ners_idx, word2idx, idx2word, ner2idx, idx2ner, char2idx, idx2char = convert_data("data/german_task_1442142996.tsv")
    sentences_tensors = get_1d_word_tensors(sentences_idx)
    ners_tensors = get_1d_word_tensors(ners_idx)

    params = {'word2idx':word2idx, 'idx2word':idx2word, 'ner2idx':ner2idx, 'idx2ner':idx2ner, 'char2idx':char2idx, 'idx2char':idx2char, 'sentences idx':sentences_idx, 'ners idx':ners_idx, 'sentences raw':sentences, 'ners raw':ners, 'sentences tensors':sentences_tensors, 'ners tensors':ners_tensors}
    torch.save(params, 'data/xlime.pt')
