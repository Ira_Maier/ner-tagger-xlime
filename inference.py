#!/usr/bin/python3
# -*- coding: utf-8 -*-

from torch.utils.data import DataLoader, Subset, ConcatDataset

#from main_model import *
from helpers import *
#from train import *
from corpora import *
from test_model import *
from flair_model import *

'''
load trained model & test it on the test set
OR
label unlabeled data
'''

if __name__ == '__main__':
    results_dir = 'results/20.07_23:35'
    checkpoint = 'checkpoint_3_61.8.pt'
    #checkpoint = 'checkpoint_0_37.53.pt'
    # results_dir = 'results/22.07_16:18'
    # checkpoint = 'checkpoint_24_56.42.pt'
    # results_dir = 'results/21.07_08:45'
    # checkpoint = 'checkpoint_4_50.56.pt'
    # results_dir = 'results/25.07_15:15'
    # checkpoint = 'checkpoint_11_64.37.pt'
    # results_dir = 'results/26.07_18:09'
    # checkpoint = 'checkpoint_4_67.08.pt'
    # results_dir = 'results/27.07_15:34'
    # checkpoint = 'checkpoint_1_57.39.pt'




    # TEST ON THE TEST SET
    print('self-calculated:', test(results_dir, checkpoint, test_list))

    # # TEST FLAIR MODEL
    # pred, prob = inference(xlime_test_flair, True)
    # f1 = f1_score(gold_test, pred)
    # f1 = round((f1 * 100), 2)
    # print('self-calculated flair:',f1)

    # LABEL UNLABELED DATA
    # ca. 15 min
    # model = crf(idx2word=idx2word, idx2char=idx2char, tag2idx=ner2idx, word_embedding=word_embedding)
    # checkpoint = torch.load(results_dir+'/'+checkpoint)
    # model.load_state_dict(checkpoint)
    # model = model.to(device)  # define model device before optimizer

    # # print(model) #for layers
    # # print('params', list(model.parameters())) # tensors
    # if next(model.parameters()).is_cuda == False: print('not running on GPU')
    #
    # model.eval()
    # preds = []
    # probs = []
    #
    # for x in api.x:
    #     if len(x) > 0:
    #         x = x.to(device)
    #         x = x.squeeze()
    #         if x.dim() == 0:
    #             x = x.unsqueeze(0)
    #         score, best_seq = model(x)
    #         preds.append(torch.tensor(best_seq,  dtype=torch.long, device=device))
    #         probs.append(score.item())
    # dict_api = {'preds':preds, 'probs':probs}
    # with open('data/twitter_api_self-labeled.pt', "wb") as f:
    #     torch.save(dict_api, f)