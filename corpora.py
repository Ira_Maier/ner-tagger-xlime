#!/usr/bin/python3
# -*- coding: utf-8 -*-

from math import ceil
from sys import getsizeof
import statistics, copy
from random import shuffle

from torch.utils.data import DataLoader, Subset, ConcatDataset
from torch.utils.data import Dataset, TensorDataset, random_split, Subset

from helpers import *
from helpers_0 import *

# todo change
dict_api_x = torch.load('data/twitter_api.pt')
#dict_api_y = torch.load('data/twitter_api_flair_labeled2tensors.pt')

# dict_api_y = open('data/twitter_api_flair-labeled.p', 'rb') #
# dict_api_y.seek(0, 0)
# dict_api_y = pickle.load(dict_api_y)

dict_api_y = torch.load('data/twitter_api_self-labeled.pt')
#dict_api_y = torch.load('data/changed_gaz_idx_flair-labeled.pt')

# dict_api_y = open('data/changed_gaz_idx_self-labeled.p', 'rb') #
# dict_api_y.seek(0, 0)
# dict_api_y = pickle.load(dict_api_y)
#
# pred =  dict_api['predictions']
# prob_xl =  dict_api['probabilities'] # contians prob for each word
# prob = [] # contains prob for each sentence
# for s in prob_xl:
#     prob.append(statistics.mean(s))

# read gazetteers
# gaz = open('data/gazetteers.p', 'rb')
# gaz.seek(0,0)
# gaz = pickle.load(gaz)


class Dataset_xlime(Dataset):
  def __init__(self):
      self.x = params_xlime['sentences tensors']
      self.y = params_xlime['ners tensors']
      # self.x = get_1d_word_tensors_in_list(params_xlime['sentences idx'])
      # self.y = get_1d_word_tensors_in_list(params_xlime['ners idx'])

  def __len__(self):
        return len(self.x)

  def __getitem__(self, idx):
      return self.x[idx], self.y[idx]

class Dataset_twitter_api(Dataset):
    '''
    idx2word contains vocab from xlime
    tweets: 0-5514
    '''
    def __init__(self):
        self.x = dict_api_x['sentences tensors'] # list of sentences as tensors, 11026
        self.y = dict_api_y['preds']
        #self.y_prob = dict_api_y['probs']
        #self.y = get_1d_word_tensors(convert_string_idx_nested(dict_api_y['predictions'], ner2idx))
        #self.mean_prob = statistics.mean(self.y_prob)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        '''
        call with: object.x[idx]
        :param idx:
        :return: sentence tensor
        '''
        return self.x[idx], self.y[idx] # , self.y_probs[idx]

def get_train_dev_test(dataset):
  split_size = int(ceil(len(dataset) / 10))  # round up
  # ratio 80/10/10
  train, dev, test = random_split(dataset, (len(dataset) - 2 * split_size, split_size, split_size))  # 2757 345 345
  return train, dev, test

def get_part_corpus(loader):
    list = []
    for x, y in loader:
        x = x.squeeze()
        y = y.squeeze()
        if (x.dim() and y.dim()) == 0:
            x, y = x.unsqueeze(0), y.unsqueeze(0)
        list.append((x, y))
    return list


# build train, dev, test sets for xlime
xlime = Dataset_xlime()
xlime_train, xlime_dev, xlime_test = get_train_dev_test(xlime) # Subsets
train_loader = DataLoader(dataset=xlime_train, shuffle=False, worker_init_fn=seed_num)
dev_loader = DataLoader(dataset=xlime_dev, shuffle=False, worker_init_fn=seed_num)
test_loader = DataLoader(dataset=xlime_test, shuffle=False, worker_init_fn=seed_num)
# convert DataLoader into list to prevent cheating
train_list_xlime = get_part_corpus(train_loader)
dev_list = get_part_corpus(dev_loader)
test_list = get_part_corpus(test_loader)

# xlime + api tweets
api = Dataset_twitter_api() #todo wieder rein
api_train_list = [(api.x[i], api.y[i]) for i in range(len(api.x))]
# #train_list_api = get_part_corpus(train_loader_api)
# #
train_list = train_list_xlime # 8368

# create subset of self-labeled tweets with probabilites higer than the mean probabilty
#idx_least_confident = [api.y_prob.index(p) for p in api.y_prob if p < api.mean_prob]

# idx_gazetteers = open('data/changed pred idx flair.p', 'rb')
# idx_gazetteers = list(pickle.load(idx_gazetteers))
# idx = idx_gazetteers+idx_least_confident
#

if __name__ == '__main__':
    pass


