from seqeval.metrics import precision_score, recall_score, accuracy_score, f1_score, classification_report
from torch.utils.data import DataLoader, Subset, ConcatDataset

from corpora import *
from main_model import *
from helpers_2 import *

def get_confidence(gold, preds, probs):
    '''
    :param gold:
    :param preds:
    :param probs:
    :return: Viterbi score of CRF
    '''
    good_probs = []
    for z in list(zip(gold, preds, probs)):
        if z[0] == z[1]:
            good_probs.append(z[2].item())
    avg_good_prob = statistics.mean(good_probs)
    low_good_prob = min(good_probs)
    max_good_prob = max(good_probs)
    return low_good_prob, avg_good_prob, max_good_prob

def calc_f1(data_list, model):
    '''

    :param data_list: test or dev set
    :param model:
    :return:

    '''
    preds = []
    gold_labels = []
    probs = []
    for x, y in data_list:
        gold_labels.append(convert_string_idx(y, idx2ner))
        with torch.no_grad():
            score, best_seq = model(x)
            preds.append(best_seq)
            probs.append(score)

    preds_string = []
    for p in preds:
        preds_string.append(convert_string_idx(p, idx2ner))

    low_prob, avg_prob, max_prob = get_confidence(gold_labels, preds_string, probs)

    # print("accuracy: {:.1%}".format(accuracy_score(gold_labels, all_preds_string)))
    # print("F1: {:.1%}".format(f1_score(gold_labels, all_preds_string)))
    print(classification_report(gold_labels, preds_string))
    f1 = f1_score(gold_labels, preds_string)

    return f1, gold_labels, preds_string, low_prob, avg_prob, max_prob


def test(results_dir, checkpoint, test_list):
    print('test: \n')
    model = crf(idx2word=idx2word, idx2char=idx2char, tag2idx=ner2idx, word_embedding=word_embedding)
    checkpoint = torch.load(results_dir+"/"+checkpoint)
    model.load_state_dict(checkpoint)

    model = model.to(device)  # define model device before optimizer
    # print(model) #for layers
    if next(model.parameters()).is_cuda == False: print('not running on GPU')
    model.eval()

    f1, gold_labels, all_preds_string, low_prob, avg_prob, max_prob = calc_f1(test_list, model)
    f1_format = "F1: {:.1%}".format(f1)
    return f1_format

    # TEST ON CONNLL FILE
    # output = open(results_dir+'/conll.txt', "w", encoding="utf-8") # todo
    # #print(output)
    # for s in list(zip(gold_labels, all_preds_string)):
    #     #print(s)
    #     for t in list(zip(s[0], s[1])):
    #         tok = 'word-fake'
    #         pos = 'POS-fake'
    #         gold = t[0]
    #         pred = t[1]  # iob
    #         output.write(tok + ' ' + pos + ' ' + gold + ' ' + pred + '\n')
    #     output.write('\n')
    # output.close()
