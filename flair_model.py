#!/usr/bin/python3
# -*- coding: utf-8 -*-
from typing import List
from pathlib import Path
import statistics

import torch
from torch.utils.data import DataLoader, Subset, ConcatDataset
import flair
from flair.data import Corpus, Sentence, Token
from flair.models import SequenceTagger
from flair.embeddings import WordEmbeddings, StackedEmbeddings, TokenEmbeddings, FlairEmbeddings, CharacterEmbeddings, BertEmbeddings, BytePairEmbeddings
from flair.trainers import ModelTrainer
from seqeval.metrics import f1_score

from corpora import *

'''
label data with flair: a NLP-Tool from Zalando Research (https://github.com/zalandoresearch/flair)
'''

flair.device = torch.device(device)

path = './flair_results'

def get_data_from_api_x(data_list):
    '''
    preprocessing for inference on unlabeled data
    convert list into list of flair sentences
    :param data_list: list of tensors
    :return:
    '''
    sentences = []
    sentence: Sentence = Sentence()

    for s in data_list:  #(tensor([  914,   189,    52,   648, 12920,  2162,  2397]), tensor([0, 0, 0, 0, 0, 0, 2]))
        for z in s:
            if z.dim() == 0:
                z = z.unsqueeze(0)
            token = Token(text=idx2word[z.item()])
            sentence.add_token(token)
        if sentence: sentences.append(sentence)
        sentence: Sentence = Sentence()
    return sentences


def get_data_from_dataset(data_list):
    '''
    convert list into list of flair sentences
    :param data_list: list of tensors
    :return:
    '''
    gold = []
    goldes = []
    sentences = []
    sentence: Sentence = Sentence()

    for s in data_list:  # (tensor([  914,   189,    52,   648, 12920,  2162,  2397]), tensor([0, 0, 0, 0, 0, 0, 2]))
        for z in list(zip(s[0], s[1])): # tensor with 1 idx
            token = Token(text=idx2word[z[0].item()].replace('#', ''))
            token.add_tag('ner', idx2ner[z[1].item()])
            gold.append(idx2ner[z[1].item()])
            if token.text == ' ': print('empty token')
            if token: sentence.add_token(token)
        if sentence: sentences.append(sentence)
        if gold: goldes.append(gold)
        sentence: Sentence = Sentence()
        gold = []
    return sentences, goldes

def model_xlime(train, dev, test):
    '''
    train flair model with xlime corpus 
    :param train: 
    :param dev: 
    :param test: 
    :return: 
    '''
    corpus: Corpus = Corpus(train, dev, test)

    tag_type = 'ner'
    tag_dictionary = corpus.make_tag_dictionary(tag_type=tag_type)

    embedding_types: List[TokenEmbeddings] = [
        #WordEmbeddings('twitter') #
        WordEmbeddings('de'), # FastText embeddings over Wikipedia
        WordEmbeddings('de-crawl'), # FastText embeddings over Web crawls
        WordEmbeddings('embeddings/spinningbytes_gensim.gensim'),
        #Spinningbytes trained with fasttext on German tweets
        FlairEmbeddings('german-forward', use_cache=True),
        FlairEmbeddings('german-backward',  use_cache=True),
    ]
    embeddings: StackedEmbeddings = StackedEmbeddings(embeddings=embedding_types) #for multiple embeddings

    pytorch_model: SequenceTagger = SequenceTagger(hidden_size=300,
                                            embeddings=embeddings,
                                            tag_dictionary=tag_dictionary,
                                            tag_type=tag_type,
                                            use_crf=True)

    pytorch_model = pytorch_model.to(device)
    if next(pytorch_model.parameters()).is_cuda == False: print('not running on GPU')

    train_model: ModelTrainer = ModelTrainer(pytorch_model, corpus)
    return pytorch_model, train_model

def train(train_model):
    train_model.train(path, mini_batch_size=32, save_final_model=True, max_epochs=50,
                  train_with_dev=False, monitor_train=True, checkpoint=True)

def inference(test_data, trained_xlime):
    '''
    takes a long time
    predict NER trained on Conll-03 OR xLiMe
    :param test_data: sentences xLiMe corpus
    :param trained_xlime: indicator, if Conll-03 or xLiMe train data should be used
    :return: nested list of tags of sentences, no flair sentences
    '''
    if trained_xlime == False:
        tagger: SequenceTagger = SequenceTagger.load('de-ner') # CoNLL03
    else:
        tagger = SequenceTagger.load(os.path.join(path,'best-model.pt'))
    sentences = []
    #prob = [] # save viterbi probability
    for s in test_data:
        #print('test sentence',s)
        #s = convert_string_idx(s, idx2word)
        tagger.predict(s)
        # predicted_y = [t.get_tag('ner')._value for t in s] # get label
        # prob_y = [t.get_tag('ner')._score for t in s] # get confidence
        # pred.append(predicted_y)
        # prob.append(prob_y)
        sentences.append(s) # contains labels and viterbi probabilities
    return sentences

# convert data into flair types
xlime_train_flair, gold_train = get_data_from_dataset(train_list)
xlime_dev_flair, gold_dev = get_data_from_dataset(dev_list)
xlime_test_flair, gold_test = get_data_from_dataset(test_list)

if __name__ == '__main__':

    #api_flair = get_data_from_api_dataset(api)

    # print(api_flair)
    # for s in api_flair:
    #     for t in s:
    #         print(t.get_tag('ner')._value)


    #pytorch_model, train_model = model_xlime(xlime_train_flair, xlime_dev_flair, xlime_test_flair) # needed for training & inference
    # print('corpus size: ', train_model.corpus)
    # print(train_model.model)
    #print(pytorch_model.model) # not working

    # RETRAIN FROM CHECKPOINT: not working
    #train_model.load_from_checkpoint(Path('flair_results/checkpoint.pt'), train_model.corpus)
    #train_model = train_model.load_from_checkpoint(os.path.join(path,'best-model.pt'), train_model.corpus)

    # TRAIN FLAIR
    #train(train_model)

    # READ CHECKPOINT
    # checkpoint = 'flair_results/54.11/best-model.pt'
    # checkpoint = torch.load(checkpoint)
    # print(train_model.corpus)

    # INFERENCE ON UNLABELED DATA
    # api_flair_x = get_data_from_api_x(api.x)
    # inference_model = pytorch_model.load(os.path.join(path,'best-model.pt'))
    # sentences = inference(api_flair_x, True)
    # with open("data/twitter_api_flair-labeled.p", "wb") as f:
    #     pickle.dump(sentences, f)

    # TRAIN FLAIR ON XLIME + API

    with open("data/twitter_api_flair-labeled.p", "rb") as f:
        sentences_api = pickle.load(f)
        print(sentences_api)
    # print('size api: ',len(sentences_api))
    # pytorch_model, train_model = model_xlime(xlime_train_flair+sentences_api, xlime_dev_flair, xlime_test_flair)
    # train(train_model)

    # flair labeled sentences 2 tensors:
    # from_flair = open('data/twitter_api_flair-labeled.p', 'rb')  #
    # from_flair.seek(0, 0)
    # from_flair = pickle.load(from_flair)
    # dict_api_y['preds'] = [[t.get_tag('ner')._value for t in s] for s in from_flair]
    # dict_api_y['preds'] = [[ner2idx[t] for t in s] for s in dict_api_y['preds']]
    # dict_api_y['preds'] = get_1d_word_tensors(dict_api_y['preds'])
    # dict_api_y['probs'] = [[t.get_tag('ner')._score for t in s] for s in from_flair]
    # dict_api_y['probs'] = [statistics.mean(s) for s in dict_api_y['probs']]
    # print(dict_api_y['probs'])
    #
    # torch.save(dict_api_y, 'data/twitter_api_flair_labeled2tensors.pt')
