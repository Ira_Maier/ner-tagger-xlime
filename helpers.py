#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os, pickle, random, itertools, sys, itertools
from datetime import datetime
from time import time
import random

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import numpy as np

#from main_model import *

'''
global variables & funtions
'''

checkpoint = 'checkpoints/checkpoint_16:06_3_59.27.pt' # first trained on xlime, then trained on both

now = datetime.now()
datum = now.strftime("%d.%m_%H:%M")
datum_short = now.strftime("%d:%m")

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#device = torch.device('cpu')

seed_num = 0
os.environ['pythonhashseed'] = str(seed_num)
random.seed(seed_num)
np.random.seed(seed_num)
torch.manual_seed(seed_num)
if torch.cuda.is_available(): torch.cuda.manual_seed_all(seed_num)
torch.backends.cudnn.deterministic=True

with open('embeddings/word embedding api.pt', 'rb') as f:
    word_embedding = torch.load(f)
    word_embedding.to(device)

# HYPERPARAMETERS:
hidden_dim = embedding_dim = word_embedding.embedding_dim
lr = 0.1
weight_decay = 1e-4

# DATA
params_xlime = torch.load(open("data/xlime.pt", "rb"))
params_api = torch.load(open('data/twitter_api.pt', 'rb'))
#pred_api = open('data/twitter api flair labeled.p', 'rb')
#pred_api = torch.load('data/twitter api self-labeled.pt', 'rb') # todo

START_TAG = "<START>"
STOP_TAG = "<STOP>"

ner2idx = params_xlime['ner2idx'] #{'O': 0, 'I-MISC': 1, 'B-PER': 2, 'I-PER': 3, 'B-LOC': 4, 'B-ORG': 5, 'I-ORG': 6, 'B-MISC': 7, 'I-LOC': 8, '<START>': 9, '<STOP>': 10}
ner2idx[START_TAG] = len(ner2idx)
ner2idx[STOP_TAG] = len(ner2idx)
idx2ner = params_xlime['idx2ner']

idx2word_xlime = params_xlime['idx2word']
word2idx_xlime = params_xlime['word2idx'] # 12923
idx2char_xlime = params_xlime['idx2char']
char2idx_xlime = params_xlime['char2idx']

idx2word_api = params_api['idx2word']
word2idx_api = params_api['word2idx']
idx2char_api = params_api['idx2char']
char2idx_api = params_api['char2idx']

# change with corpus size
idx2word = idx2word_api # 84206
idx2char = idx2char_api # 912
word2idx = word2idx_api
char2idx = char2idx_api

# idx2word = idx2word_xlime
# idx2char = idx2char_xlime
# word2idx = word2idx_xlime
# char2idx = char2idx_xlime

def convert_string_idx(seq, dict):
    '''
    converts strings to idx representations or vice versa
    :param seq: list of strings or idx
    :param dict: key: string, value: idx or vice versa
    :return: list of idx or strings
    '''
    new_type = []
    for w in seq:
        if isinstance(w, str) == True or isinstance(w, int) == True: # if element is a string or a number
            new_type.append(dict[w])
        else:
            new_type.append(dict[w.item()]) # if element is a tensor
    return new_type

def convert_string_idx_nested(seq, dict):
    '''
    same as convert_string_idx, but with nested lists
    :param seq: nested list of strings or idx
    :param dict: key: string, value: idx or vice versa
    :return:
    '''
    new_seq = []
    for s in seq:
        new_type = []
        for w in s:
            if isinstance(w, str) == True:
                new_type.append(dict[w])
            else:
                new_type.append(dict[w.item()]) # if element is a tensor
        new_seq.append(new_type)
    return new_seq

def get_1d_word_tensors_in_list(seq):
    '''

    :param seq: list with list of indices inside
    :return: list with tensors inside
    '''
    back = []
    for s in seq:
        #print(torch.tensor(s, dtype=torch.long, device=device))
        back.append(torch.tensor(s, dtype=torch.long, device=device))
    return back

if __name__ == '__main__':
    pass
