#!/usr/bin/python3
# -*- coding: utf-8 -*-
from time import time

from helpers import *

START_TAG = "<START>"
STOP_TAG = "<STOP>"

#device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def get_false_tagged(seq, gold, predicted):
    '''
    compares predicted and gold labels to find which named entities are wrong labeled
    :param seq: list of sentences
    :param gold: list of correct labels for seq
    :param predicted: list of predicted labels for seq
    :return: list of tuples with words for which gold and predicted labels are different
    '''
    zipped = list(zip(seq, gold, predicted))  # word, gold label, predicted label
    false_tagged = []
    for z in zipped:
        zipped = list(zip(z[0], z[1], z[2]))
        for w in zipped:
            if w[1] != w[2]:
                false_tagged.append(w)
    return false_tagged

def argmax(vec):
    # return the argmax as a python int
    _, idx = torch.max(vec, 1)
    return idx.item()

# Compute log sum exp in a numerically stable way for the forward algorithm
def log_sum_exp(vec):
    max_score = vec[0, argmax(vec)]
    max_score_broadcast = max_score.view(1, -1).expand(1, vec.size()[1])
    return max_score + torch.log(torch.sum(torch.exp(vec - max_score_broadcast))) #no device

class crf(nn.Module):
    '''
    from https://pytorch.org/tutorials/beginner/nlp/advanced_tutorial.html
    hidden dim must be equal to embedding dim
    '''
    def __init__(self, idx2word, idx2char, tag2idx, word_embedding):
        super(crf, self).__init__()
        self.idx2word = idx2word
        self.idx2char = idx2char
        self.char2idx = {idx: w for w, idx in self.idx2char.items()}
        self.tag2idx = tag2idx
        self.tagset_size = len(self.tag2idx)

        self.word_embedding = word_embedding #pretrained word embeddings
        self.word_embedding.weight.requires_grad = True # True = trainable, unfrozen
        self.hidden_dim = self.word_embedding.embedding_dim
        embedding_dim = self.word_embedding.embedding_dim
        #print('trainable?',self.word_embedding.weight.requires_grad)
        #embedding_dim = self.word_embedding.embedding_dim #vocab size * 2400
        #hidden_dim = self.word_embedding.embedding_dim #600
        self.char_embedding = nn.Embedding(len(idx2char), embedding_dim)
        self.char_embedding.weight.requires_grad = True
        self.char_lstm = nn.LSTM(embedding_dim, self.hidden_dim // 2, num_layers=1, bidirectional=True)
        self.word_lstm = nn.LSTM(embedding_dim + self.hidden_dim, self.hidden_dim // 2, num_layers=1, bidirectional=True)
        self.hidden2tag = nn.Linear(self.hidden_dim, self.tagset_size) # hidden2tag
        self.dropout = torch.nn.Dropout(p=0.5)

        self.transitions = nn.Parameter(
            torch.randn(self.tagset_size, self.tagset_size, device=device)) #shape 11, 11, torch.device("cuda")

        # These two statements enforce the constraint that we never transfer
        # to the start tag and we never transfer from the stop tag
        self.transitions.data[self.tag2idx["<START>"], :] = -10000
        self.transitions.data[:, self.tag2idx["<STOP>"]] = -10000

        self.hidden = self.init_hidden()

    def init_hidden(self):
        return (torch.randn(2, 1, self.hidden_dim // 2).to(device),
                torch.randn(2, 1, self.hidden_dim // 2).to(device))

    def _forward_alg(self, feats):
        t1 = time()
        # Do the forward algorithm to compute the partition function
        init_alphas = torch.full((1, self.tagset_size), -10000., device=device) # filled with -10000
        # START_TAG has all of the score.
        init_alphas[0][self.tag2idx[START_TAG]] = 0.

        # Wrap in a variable so that we will get automatic backprop
        forward_var = init_alphas

        # Iterate through the sentence
        for feat in feats:
            alphas_t = []  # The forward tensors at this timestep
            for next_tag in range(self.tagset_size):
                # broadcast the emission score: it is the same regardless of
                # the previous tag
                emit_score = feat[next_tag].view(1, -1).expand(1, self.tagset_size)
                emit_score = emit_score.to(device)
                # the ith entry of trans_score is the score of transitioning to
                # next_tag from i
                trans_score = self.transitions[next_tag].view(1, -1)
                trans_score = trans_score.to(device)
                # The ith entry of next_tag_var is the value for the
                # edge (i -> next_tag) before we do log-sum-exp
                next_tag_var = forward_var + trans_score + emit_score
                next_tag_var = next_tag_var.to(device)
                # The forward variable for this tag is log-sum-exp of all the
                # scores.
                alphas_t.append(log_sum_exp(next_tag_var).view(1))
            forward_var = torch.cat(alphas_t).view(1, -1).to(device)
            forward_var = forward_var.to(device)
        terminal_var = forward_var + self.transitions[self.tag2idx[STOP_TAG]]
        alpha = log_sum_exp(terminal_var)
        #print('time _forward_alg: ', (round(time()-t1))/60)
        return alpha

    def _get_lstm_features(self, sentence):
        '''
        get character + word features
        :param sentence:
        :return:
        '''
        self.hidden = self.init_hidden()
        embeds = self.word_embedding(sentence).view(len(sentence), 1, -1) #Tensor shape: len(sentnece), 1, hidden dim
        #embeds = self.dropout(embeds)

        char_hidden_final = []

        for word in sentence:
            word_idx = word.item()
            word = self.idx2word[word_idx]
            char_tensor = [self.char2idx[i] for i in word]
            char_tensor = torch.tensor(char_tensor, dtype=torch.long, device=device)
            char_embeds = self.char_embedding(char_tensor) #shape len(sentence), embedding dim
            char_embeds = self.dropout(char_embeds)
            _, (char_hidden, char_cell_state) = self.char_lstm(char_embeds.view(len(char_tensor), 1, -1)) #char hidden shape: 2, 1, 1200
            #char_hidden = self.dropout(char_hidden)
            word_char_hidden_state = char_hidden.view(-1)  # shape: 600
            char_hidden_final.append(word_char_hidden_state)
        char_hidden_final = torch.stack(tuple(char_hidden_final)) #shape len(sentence), embedding dim
        char_hidden_final = char_hidden_final.unsqueeze(1) #add 1 dim
        combined = torch.cat((embeds, char_hidden_final), 1)

        lstm_out, self.hidden = self.word_lstm(combined.view(len(sentence), 1, -1), self.hidden) #seq_len, batch, input_size
        #lstm_out = self.dropout(lstm_out)
        lstm_out = lstm_out.view(len(sentence), self.hidden_dim)
        lstm_feats = self.hidden2tag(lstm_out) #linear layer: shape len(sentence), tagset size
        #lstm_feats = self.dropout(lstm_feats)
        #print('time _get_lstm_features: ', (round(time()-t2)) / 60)
        return lstm_feats

    def _score_sentence(self, feats, tags):
        # Gives the score of a provided tag sequence

        score = torch.zeros(1).to(device)
        tags = torch.cat([torch.tensor([self.tag2idx[START_TAG]], dtype=torch.long, device=device), tags.to(device)])
        for i, feat in enumerate(feats):
            score = score + self.transitions[tags[i + 1], tags[i]] + feat[tags[i + 1]]
        score = score + self.transitions[self.tag2idx[STOP_TAG], tags[-1]]
        #print('time _score_sentence: ', (round(time()-t3)) / 60)
        return score

    def _viterbi_decode(self, feats):
        backpointers = []

        # Initialize the viterbi variables in log space
        init_vvars = torch.full((1, self.tagset_size), -10000., device=device) # torch.device("cuda")
        init_vvars[0][self.tag2idx[START_TAG]] = 0

        # forward_var at step i holds the viterbi variables for step i-1
        forward_var = init_vvars
        for feat in feats:
            bptrs_t = []  # holds the backpointers for this step
            viterbivars_t = []  # holds the viterbi variables for this step

            for next_tag in range(self.tagset_size):
                # next_tag_var[i] holds the viterbi variable for tag i at the
                # previous step, plus the score of transitioning
                # from tag i to next_tag.
                # We don't include the emission scores here because the max
                # does not depend on them (we add them in below)
                next_tag_var = forward_var + self.transitions[next_tag]
                best_tag_id = argmax(next_tag_var)
                bptrs_t.append(best_tag_id)
                viterbivars_t.append(next_tag_var[0][best_tag_id].view(1))
            # Now add in the emission scores, and assign forward_var to the set
            # of viterbi variables we just computed
            forward_var = (torch.cat(viterbivars_t) + feat).view(1, -1)
            backpointers.append(bptrs_t)

        # Transition to STOP_TAG
        terminal_var = forward_var + self.transitions[self.tag2idx[STOP_TAG]]
        best_tag_id = argmax(terminal_var)
        path_score = terminal_var[0][best_tag_id]

        # Follow the back pointers to decode the best path.
        best_path = [best_tag_id]
        for bptrs_t in reversed(backpointers):
            best_tag_id = bptrs_t[best_tag_id]
            best_path.append(best_tag_id)
        # Pop off the start tag (we dont want to return that to the caller)
        start = best_path.pop()
        assert start == self.tag2idx[START_TAG]  # Sanity check
        best_path.reverse()
        #print('time viterbi: ', (round(time()-t4)) / 60)
        return path_score, best_path

    def neg_log_likelihood(self, sentence, tags):
        # during training
        # print('sent',sentence)
        # print('tags',tags)
        feats = self._get_lstm_features(sentence)
        feats = feats.to(device)
        forward_score = self._forward_alg(feats)
        gold_score = self._score_sentence(feats, tags)
        return forward_score - gold_score

    def forward(self, sentence):
        # during evaluation
        # dont confuse this with _forward_alg above.
        # Get the emission scores from the BiLSTM
        lstm_feats = self._get_lstm_features(sentence)
        # Find the best path, given the features.
        score, tag_seq = self._viterbi_decode(lstm_feats)
        return score, tag_seq