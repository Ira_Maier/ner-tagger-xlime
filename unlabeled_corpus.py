#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pickle

from helpers_1 import *

'''
prepare unlabeled tweets from twitter api:
1) open unlabeled tweets
2) tokenizing & removing, replacing
3) create sentences idx, ners idx, word2idx & chard2idx with words from unlabeled tweets
'''

out_filename = 'data/twitter_api.pt'
lines = torch.load(open('data/tweets_corpus.pt', 'rb'))

word2idx = params_xlime['word2idx']
char2idx = params_xlime['char2idx'] # erweitert

def get_tweets(lines):
    '''
    :param in_filename: file with raw tweets
    :return: tweets in flair Sentence format
    '''

    sentenceS_idx = []
    sentenceS_raw = []
    #sentence: Sentence = Sentence()


    for l in lines:
        l = l.split()
        k = []
        for w in l:
            w = w.strip().replace('#', '')
            if w:
                if w.startswith('https://'):
                    w = 'TURLURL'
                    k.append(w)
                if w.startswith('@'):
                    w = 'TUSERUSER'
                    k.append(w)
                if w.endswith('...'): # split ...
                    k.append(w[:-4])
                    k.append('...')
                elif w[-1] in ['?', '!', '.', ';', ':', ',', '-']: # split punctuation marks from string
                    k.append(w[:-1])
                    k.append(w[-1])
                    del w
                else:
                    if w and w.isspace() == False:
                        k.append(w)
        k = [e for e in k if e] # remove empty strings
        sentence_idx = []
        sentence_raw = []
        for w in k: #word
            # split punctuation marks
            if w not in word2idx:
                #print('word',w)
                word2idx[w] = len(word2idx)
            sentence_idx.append(word2idx[w])
            sentence_raw.append(w)
            for c in w: # char
                if c not in char2idx:
                    char2idx[c] = len(char2idx)
        if sentence_idx and sentence_raw:
            sentenceS_idx.append(sentence_idx)
            sentenceS_raw.append(sentence_raw)
            sentence_idx, sentence_raw = [], []
    return sentenceS_idx, sentence_raw

if __name__ == '__main__':
    params = {}
    params['sentences idx'], params['sentences raw'] = get_tweets(lines)
    params['sentences tensors'] = get_1d_word_tensors(params['sentences idx'])
    params['word2idx'] = word2idx
    params['idx2word'] = {idx: w for w, idx in word2idx.items()}
    params['char2idx'] = char2idx # 950
    params['idx2char'] = {idx: w for w, idx in char2idx.items()}

    torch.save(params, out_filename)

    # with open(out_filename, "wb") as f:
    #     pickle.dump(params, f)
