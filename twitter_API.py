import tweepy
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json

import twitter_credentials

'''
1) Create a class inheriting from StreamListener
In Tweepy, an instance of tweepy.Stream establishes a streaming session and routes messages to StreamListener instance. 
The on_data method of a stream listener receives all messages and calls functions according to the message type. 
2) Using that class create a Stream object
3) Connect to the Twitter API using the Stream.
'''

#override tweepy.StreamListener to add logic to on_status
class MyStreamListener(tweepy.StreamListener):
    '''
    connects to api
    '''
    def on_status(self, status):
        print(status.text)

    def on_data(self, data):
        deserialized = json.loads(data)  #data = string, deserialized = dict
        if 'extended_tweet' in deserialized:
            text = deserialized['extended_tweet']['full_text']
        else:
            text = deserialized['text']
        with open('data/tweets_de.txt','a') as tf:
            tf.write(text)
        return True

    def on_error(self, status):
        print(status)

if __name__ == '__main__':
    #3)
    auth = tweepy.OAuthHandler(twitter_credentials.consumer_key, twitter_credentials.consumer_secret)
    auth.set_access_token(twitter_credentials.access_token, twitter_credentials.access_token_secret)

    myStreamListener = MyStreamListener()
    myStream = tweepy.Stream(auth=auth, listener=myStreamListener)
    myStream.filter(languages=["de"], track='Merkel') # sometimes you need the parameter track,
    # but the streams will not be searched for tweets containing the track words, so the track function is not really working ;)
    #print(myStreamListener.body())