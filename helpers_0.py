#!/usr/bin/python3
# -*- coding: utf-8 -*-
import torch
import torch.backends.cudnn as cudnn

cudnn.deterministic = True
cudnn.benchmark = True
cudnn.enabled = True
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#device = torch.device('cpu')
#CUDA_LAUNCH_BLOCKING=1 # for better error messages, but slow


def convert_string_idx_nested(seq, dict):
    '''
    same as convert_string_idx, but with nested lists
    :param seq:
    :param dict:
    :return:
    '''
    new_seq = []
    for s in seq:
        new_type = []
        for w in s:
            if isinstance(w, str) == True:
                new_type.append(dict[w])
            else:
                new_type.append(dict[w.item()]) # if element is a tensor
        new_seq.append(new_type)
    return new_seq

def get_1d_word_tensors(seq):
    '''
    :param seq: list of words in a sentence
    :return: list of tensors
    '''
    return [torch.tensor(s,  dtype=torch.long, device=device) for s in seq]